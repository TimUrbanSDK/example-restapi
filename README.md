# Example RestAPI

Frameworks: Flask or Tornado
Database: Postgres with PostGIS extension

Project Structure
  - app.py
  - config (yaml, json, text is fine)
  - requirments.txt
  - /project (for everything else)

Data: data.zip
Using the provided data create a rest API that allows the user to query incidents by county and datetime

Extra Point:
Create a front end display to show the incidents using mapbox https://www.mapbox.com/
Docker File to build project in a container

